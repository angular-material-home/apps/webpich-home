# Angular Material Home Shop (Webpich Home)

[![pipeline status](https://gitlab.com/angular-material-home/apps/webpich-home/badges/master/pipeline.svg)](https://gitlab.com/angular-material-home/apps/webpich-home/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/4bd01f8b645b4873b966e9c17a4e0024)](https://www.codacy.com/app/amh-apps/webpich-home?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=angular-material-home/apps/webpich-home&amp;utm_campaign=Badge_Grade)

It is an Angular Material SPA for selling products and services. It supports main functionlity of Shop module of Seen servers. This is the default SPA for tenants on webpich.com.