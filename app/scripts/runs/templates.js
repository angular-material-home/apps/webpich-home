/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('webpichHome')

.run(function($wbUi) {
	/*
	 * Register a tips 
	 */
	$wbUi.newTemplate({
		name : 'Chershoee',
		description: 'Landing page of your shop',
		thumbnail : 'resources/templates/chershoee/thumb.jpg',
		templateUrl: 'resources/templates/chershoee/main.wb',
		language: 'en',
		priority: 10
	})
	.newTemplate({
		name : 'Portfolio',
		description: 'Portfolio template',
		thumbnail : 'resources/templates/portfolio/thumbnail.png',
		templateUrl: 'resources/templates/portfolio/main.json',
		language: 'fa',
		priority: 10
	})
	.newTemplate({
		name : 'Classic',
		description: 'Classic page',
		thumbnail : 'resources/templates/classic/thumbnail.png',
		templateUrl: 'resources/templates/classic/main.json',
		language: 'en',
		priority: 100
	})
        .newTemplate({
		name : 'Projection',
		description: 'Projection',
		thumbnail : 'resources/templates/projection/projection.png',
		templateUrl: 'resources/templates/projection/projection.json',
		language: 'en',
		priority: 10000
	})
        .newTemplate({
		name : 'Wirework',
		description: 'Wirework',
		thumbnail : 'resources/templates/wirework/wirework.png',
		templateUrl: 'resources/templates/wirework/wirework.json',
		language: 'en',
		priority: 100
	})
        .newTemplate({
		name : 'hongkong traffic',
		description: 'hongkong traffic',
		thumbnail : 'resources/templates/hongkong traffic/hongkong traffic.png',
		templateUrl: 'resources/templates/hongkong traffic/hongkong traffic.json',
		language: 'en',
		priority: 100
	})
        .newTemplate({
		name : 'monochromed',
		description: 'hongkong traffic',
		thumbnail : 'resources/templates/monochromed/monochromed.png',
		templateUrl: 'resources/templates/monochromed/monochromed.json',
		language: 'en',
		priority: 100
	})
        .newTemplate({
		name : 'prophyrio',
		description: 'hongkong traffic',
		thumbnail : 'resources/templates/prophyrio/prophyrio.png',
		templateUrl: 'resources/templates/prophyrio/prophyrio.json',
		language: 'en',
		priority: 100
	})
        .newTemplate({
		name : 'phaseshift',
		description: 'hongkong traffic',
		thumbnail : 'resources/templates/phaseshift/phaseshift.png',
		templateUrl: 'resources/templates/phaseshift/phaseshift.json',
		language: 'en',
		priority: 100
	})
        .newTemplate({
		name : 'snapshot',
		description: 'hongkong traffic',
		thumbnail : 'resources/templates/snapshot/snapshot.png',
		templateUrl: 'resources/templates/snapshot/snapshot.json',
		language: 'en',
		priority: 100
	})
});

