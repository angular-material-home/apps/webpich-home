'use strict';

/**
 * @ngdoc overview
 * @name webpichHome
 * @description # webpichHome
 * 
 * Main module of the application.
 */
angular.module('webpichHome', [
	'seen-shop',//
	'mblowfish-language',
	
	'ngMaterialHomeUser', //
	'ngMaterialHomeSpa', //
	'ngMaterialHomeTheme', //
	'ngMaterialHomeBank', //
	'ngMaterialHomeShop' //
])//
.config(function($localStorageProvider) {
	$localStorageProvider.setKeyPrefix('webpichHome.');
})
.run(function ($app, $toolbar) {
	// start the application
	$toolbar.setDefaultToolbars(['amh.owner-toolbar']);
	$app.start('app-webpich-home');
});